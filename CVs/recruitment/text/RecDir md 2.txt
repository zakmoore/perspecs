 Contact
07876236997 (Mobile) simon.shobrook@investigo.co .uk
www.linkedin.com/in/shobrook
(LinkedIn)
www.fuelrecruitment.co.uk/
(Company) pivot-rpo.co.uk/ (Company)
Top Skills
Contract Recruitment Permanent Placement Head Hunter
Simon Shobrook
Associate Director at Investigo Public Sector 07876236997 simon.shobrook@investigo.co.uk
United Kingdom
Summary
I have successfully delivered fixed price digital solutions (including GDS assessed government services), contract resources, executive search assignments, designed and executed permanent campaigns.
Examples include, niche cyber security assignments for DV cleared Defence and Security environments, press campaigns, bi-lingual international resources, partner level search and complete project teams.
I have opened new major accounts through exec-level sponsorship (Big4, mid-tier consultancies, leading IT&T organisations and public sector organisations). Designed and implemented successful delivery models. Developed new product offerings, built teams and turned around under-performers. Succession planning and career development for my team members. Successful bid writer, from retained search to major framework wins.
- Consulting, Technology and Digital Transformation
- Advisory services for customers
- Partnership approach with customers to support resource planning and brand development in the market
- Resource planning and talent pipe-lining
- Campaign design
- Career coaching
During my career, I have built up a wide-ranging network of trusted contacts:
Agile Delivery, BI, Big Data, BPR, Business Analyst, Business Continuity, Change Manager, CHECK, CIO, CDO, CISO, CTO,
CxO, CISSP, CLAS, Cloud, Content Designer, Cyber Security, Data Analytics, Developer (Java, .Net, C#, C++, Node.js, PHP, Scala), Developed Vetting (DV), Drupal, E-Commerce, ediscovery, G- Cloud, IaaS, InfoSec, Infrastructure, Intelligence, Interim Manager, IT
Page 1 of 5

 Forensics, LEAN, Managing Consultant, Marketing, Network, PaaS, Partner, Penetration Testing, Performance Analyst, Procurement, Programme Manager, Project Manager, PMO, PSO, Risk, SC Cleared, Scrum Master, Security Clearance, Service Architect, Smart Metering, Storage, STRAP, Supply Chain, Target Operating Model, Technical Architect, Technical Assurance, Tester, Trainer, User Researcher, Virtualisation (VMWare & Hyper-V).
Experience
Investigo Public Sector
Associate Director - IT & Digital - Change, Transformation and Delivery January 2018 - Present (2 years 10 months)
London, United Kingdom
A delivery partner for government technology, change and transformation projects.
G-Cloud DOS NMNC Bloom Insight ESPO YPO
Investigo Public Sector deliver consulting engagements to our end client, offering quality of service and value for money. We are experts in delivery across Public Sector with a niche knowledge of central government, borne from more than 20 years� experience. We are a trusted delivery partner offering fully managed services, as well as tailored solutions to our client base.
We deploy lean teams to support the delivery of projects and programmes under the direction and control of civil service stakeholders. As such we have supported clients in delivery of GMPP scale programmes resulting in IPA green reports as well as shorter term change consultancy projects.
Investigo
Associate Director
January 2018 - Present (2 years 10 months) London, United Kingdom
 Page 2 of 5

 Helping organisations deliver digital and technology transformation projects.
Fixed price, outcome based delivery, and more traditional interim teams on a T&M basis.
Digital Transformation
Discovery and user research DevOps
Security
Technical Architecture
Technical Assurance
Deployment and Testing
Agile Software Delivery Teams Project and Change Management
Government Supplier Frameworks:
NMNC - IT, Corporate Functions and Admin
CL1 - Digital, IT, Change and Commercial Specialists DOS 2 - Digital Outcomes and Specialists
G-Goud - Cloud and Digital Transformation
Family time
On gardening leave
October 2017 - December 2017 (3 months)
The Fuel Group
Head of Digital & Consulting
October 2012 - December 2017 (5 years 3 months) London, United Kingdom
Leading The Fuel Group's London Office, with a focus on delivering value to customers through Fuel Professional Services. Helping organisations deliver digital and technology transformation projects.
Fixed price outcome based delivery and more traditional interim teams on a T&M basis.
Digital Transformation Discovery and user research Security
Page 3 of 5

 Technical Architecture Technical Assurance Deployment and Testing
Agile Software Delivery Teams
Crown Commercial Service supplier through G-Cloud 9 and DOSII (Digital Outcomes and Specialists).
300 technical and functional consultants working on customer sites, including over 100 with SC or DV security clearance.
The Fuel Group has three service offerings:
1. Fuel Professional Services: Digital transformation, software and systems delivery.
2. Fuel Recruitment: Contract, Permanent and Search - IT, Telecoms. Engineering and Marketing.
3. Pivot RPO: Recruitment Outsourcing Solutions.
CDI
Divisional Manager
November 2010 - October 2012 (2 years)
Responsible for market identification, business strategy and client engagement. Developed and led skills-driven recruitment solutions for our customers throughout EMEA. I provided niche recruitment expertise and managed experienced teams of specialist recruiters working in niche vertical skills markets.
Business Change & Consulting: Contract and Permanent CxO & Snr Management: Interim and Search
IT, Technology & New Media: Contract and Permanent
Project work:
Expansion of CDI�s Defence & Homeland Security Practice in the UK; recruitment services for niche SC & DV security cleared environments. Developing CDI's UK E-Commerce recruitment capability.
Hudson
Managing Consultant
October 2003 - November 2010 (7 years 2 months)
Page 4 of 5

 Hudson Global Resources
Hudson (NASDAQ: HHGP) is a leading provider of permanent recruitment, contract professionals and talent management solutions worldwide. The company employs nearly 2500 professionals serving clients and candidates in more than 20 countries.
Hudson�s provides recruitment services in the following specialist fields:
� IT & Telecoms
� HR
� Accounting & Finance
� Legal
� PR, Marketing, Communications & Customer Services � Supply Chain & Procurement
� Estates, Facilities & Capital Projects
� Commissioning
� Technical Operations & General Management
� Exec & Search
Specialties:Contract, Interim and Permanent/Search recruitment solutions. Professional services and Management Consultancies. Central Government, NDPBs, NHS, Charities and Not for Profit.
Glotel
Recruiter
June 2000 - September 2003 (3 years 4 months)
GS Shobrook & Co
Trainee Surveyor
July 1998 - June 2000 (2 years)
Education
University of Portsmouth
BSc Land Management � (1994 - 1998)
 Page 5 of 5
