 Contact
alex.mauree@franklinbates.c om
www.linkedin.com/in/alexmauree
(LinkedIn) www.franklinbates.com (Other)
Top Skills
Recruiting Management Talent Management
Languages
French
Alex Mauree
Director of Executive Search & Selection
London, England Metropolitan Area
Summary
Having joined the industry in 1997, I have been privileged to experience a variety of market conditions during my career.
All effective relationships in life depend upon certain tenets - trust, honesty, integrity, compassion and understanding. It is only when these are truly achieved that the relationship becomes a partnership. Through your dealings with Franklin Bates we hope you will clearly see these are values we are happy to be judged against. Our aim
is to be seen as the recruitment service provider of choice, and we wish to achieve this through demonstrating professionalism, passion and pride in what we do.
Clients - Our ambition is to use our expertise to provide solutions to your business resourcing needs. Our focus is building high performing cultures encompassing both diversity and inclusion.
Candidates - Whatever your reason for change - challenge, growth, opportunity, technology or reward - we will understand, advise, support and guide you towards the right organisation, for the right reason.
Specialities: Executive Recruitment, Executive Coaching, Recruitment Strategy, Business Strategy, Leadership training, Building Diverse Teams
Experience
Franklin Bates
Managing Director
January 2015 - Present (5 years 10 months) Theale, Berkshire, UK
 Page 1 of 2

 Franklin Bates is a specialist IT Recruitment business with a proven track record of successfully delivering contract, permanent and temporary services across a broad range of clients, brands and business verticals.
Our success is based on the value we place on our clients, candidates and staff. We pride ourselves on being able to deliver high quality bespoke solutions to our clients understanding the value of the projects, business objectives and their brand in the market place they operate within.
Franklin Bates� strength lies in the highly motivated, and on-going training, of our recruitment professionals to become specialists in their market places and thus become adept at identifying and sourcing those �hard to find� candidates and skillsets.
Connections Recruitment, a YOH Company Managing Director
August 1997 - December 2014 (17 years 5 months)
At Connections our aim is simple - to provide the whole life-cycle guidance for recruitment within the IT and sales and marketing sectors. With a wealth of experience gained since Connections was established in 1988, we have developed into a company that provides a range of tailored recruitment services to meet the challenges of today's business and technology patterns.
Connections has built a prestigious and diverse client base encompassing
all areas of the IT and sales and marketing industries, including all the major manufacturers, software vendors, systems integrators, consultancies and end- users.
Many organisations have found our expertise in these sectors to be unique.
Connections achieves working partnerships with candidates and clients through demonstrating passion and pride in what we do, and placing high value on trust, honesty, integrity, compassion and understanding.
Education
Henley Management College Organizational Leadership � (2011)
 Page 2 of 2
