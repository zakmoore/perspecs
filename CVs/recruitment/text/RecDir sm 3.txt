 Contact
207 Regent Street
London
W1B3HH
07774626249 (Mobile) simon.voice@franklinbates.c om
www.linkedin.com/in/simonvoice
(LinkedIn) www.franklinbates.com (Blog)
Top Skills
Agile Methodologies Scrum
Recruiting
Honors-Awards
Henley Business Business - Leadership Course
Simon Voice Franklin Bates Ltd
United Kingdom
Summary
I'm generally recognised as the pioneer of Agile Recruitment in
the UK since delivering a team of XP consultants to a consulting business back in 1999. Since then I have been a significant contributor to projects such as Specsavers, Lastminute.com, Dixons, AOL, BSkyB, BBC, Brighthouse, Laterooms, and DWP amongst others. Community and People are key to the success of what
has been achieved, demonstrated by being the only recruiter sponsoring and helping with the Agile Business Conference and in latter years launching the UK Agile Awards which was my brainchild in its inaugural year in 2010. Very frequently I'm often involved with facilitating the recovery of failing projects and getting new projects launched and safely underway.
Experience
Franklin Bates Ltd
Director
July 2013 - Present (7 years 4 months) UK
For 14 years the ethos of my work has been very much the same from day one. I very much subscribe to the idea that recruitment is all about the people. The simple formula that I share with everyone is: Relationship + Opportunity + Quality = Success
Over the last 14 years Agile Development & Business Methods has become part of the day to day world we live in and with this in mind, Franklin Bates Ltd will continue to work with individuals and organisations where the emphasis
is always about the people and culture within an Agile environment. We
offer more than just a staffing solution as we are able to flex to the demands and challenges that every hiring organisation encounters; whether that is permanent, contract, project teams, training & coaching or alternatively as a retained outsource option. Franklin Bates Ltd was formed to give the highest quality service possible, without losing the fun and the focus on the client and candidate interaction. We look forward to hearing from you.
 Page 1 of 3

 Connections Recrutiment / Yoh Ltd Agile Specialist
January 2012 - June 2013 (1 year 6 months) UK, USA, Europe
Very simply my role is to meet clients, candidates and interested parties surrounding the Agile world and community. I help them to source, structure and enable them to build their business's and projects with the right people with the right skills. I champion the Agile community running events, forums, challenging the status quo and evolving new ways of helping and putting the right people together. Very simple, very time consuming - VERY REWARDING !
Connections Recruitment 13 years
Agile Practice Manager
January 2005 - December 2011 (7 years) Berkshire
For 6 years the growth, enablement and evolvement of the Agile Community in the UK has come from within the Agile Team(s) that I have put together
at Connections. Without doubt we have, are the prominant supplier in UK of high quality Agile talent whether this is within development, infrastructure, enablement, transformation, coaching, training or mentoring. The success
has additionally come from being part of the Agile Business Conference since 2007, building and evolving for the last 3 years the Agile Awards and hosting ongoing networking events that has enabled us to build the largest network and database of Agile people in the UK. Due to the ongoing global demand for Agile specialist's we have been for the last 2 years working with companies and organisations on a global basis. Simply if you have an Agile problem or need WE CAN HELP YOU !
Agile Account & Candidate Manager January 1999 - January 2005 (6 years 1 month) Berkshire
In 1999 I first placed the first XP team of developers into now one of the most successful online business's in Europe. From this point onwards the supply, generation and placing of candidates into Agile projects became an everyday occurence. For 6 years I learnt my trade, my network, my people and my specilaism.
 Page 2 of 3

 Education
Tomlinscote
� (1980 - 1992)
Ravenscote
� (1980 - 1984)
Page 3 of 3
