 Contact
www.linkedin.com/in/joe- fitter-64701127 (LinkedIn)
Top Skills
JavaScript
Backbone.js Backbone.Marionette.js
Certifications
ScrumMaster
Joe Fitter
Javascript Developer at Marvell Consulting Limited
Greater Brighton and Hove Area
Experience
Marvell Consulting Limited
Contract Software Engineer
March 2018 - Present (2 years 8 months) London, United Kingdom
UK Home Office
Contract JavaScript Consultant April 2016 - March 2017 (1 year) Croydon, United Kingdom
pebble {code}
JavaScript Developer
June 2014 - March 2016 (1 year 10 months) London, United Kingdom
Bede Gaming Ltd
Scrum Master / JavaScript Developer June 2014 - March 2016 (1 year 10 months)
Joe Fitter
Freelance Web Application Developer June 2010 - August 2015 (5 years 3 months)
iCrossing UK
Front End Developer
March 2012 - May 2014 (2 years 3 months)
Rebob/Yellow60
Freelance Web Developer
November 2010 - June 2013 (2 years 8 months)
 Page 1 of 1
