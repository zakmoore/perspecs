 Contact
www.linkedin.com/in/marvell
(LinkedIn)
www.marvell-consulting.com
(Company)
Top Skills
node.js JavaScript HTML5
Ben Marvell
Founder & CEO at Marvell Consulting Limited
United Kingdom
Summary
Ben is a development consultant with particular experience in digital transformation. He has worked in numerous environments in varying team sizes (1�30+) for clients such as: EA, Centrica, BSkyB, John Wiley & Sons, LexisNexis and the Government. Ben is a passionate developer and is active in the community with particular interest in emerging technologies.
Experience
Marvell Consulting Limited
Chief Executive Officer
March 2007 - Present (13 years 8 months) London
Marvell Consulting Limited transforms the digital services of both public and private sector clients. Our multi-disciplinary team put users at the heart of everything we do to create digital services that directly address, and anticipate, their needs.
HM Passport Office
Full Stack Javascript Developer & WebOps October 2017 - October 2018 (1 year 1 month) London, United Kingdom
UK Home Office
Full Stack Javascript Developer & Consultant June 2015 - September 2017 (2 years 4 months)
Government Digital Service
Javascript Developer
January 2014 - June 2015 (1 year 6 months) London
Centrica
JavaScript Developer & Consultant
 Page 1 of 4

 February 2013 - December 2013 (11 months)
London
Worked on the team responsible for creating the web application and brochure site for hivehome.com
Web development using Node.JS, Express, Redis, Backbone JS, Mocha and Chai for TDD, WebDriver and Selenium for functional testing. Extensive use of external APIs including integration with existing Salesforce platform.
Electronic Arts
JavaScript Developer & Consultant October 2012 - February 2013 (5 months)
Working on a pure JavaScript mobile application.
Technologies include: Backbone JS, RequireJS, Zepto, Underscore, Grunt, Jasmine, Cucumber JS, WebDriverJs
Burberry
JavaScript Developer & Consultant August 2012 - September 2012 (2 months)
Working on Burberry World a portal/social network for the Burberry Brand. Technologies include: JavaScript, jQuery, RequireJS, CSS, Salesforce
LexisNexis
JavaScript Developer & Consultant March 2012 - September 2012 (7 months) Camden
Porting a silverlight application to JavaScript. Applied Document Driven Development (DDD) throughout the project.
Technologies include: JavaScript, Backbone JS, Raphael JS, jQuery, Underscore, Git, Cucumber JS, WebDriverJs.
BSkyB
JavaScript (node.js) Developer & Consultant May 2011 - March 2012 (11 months)
Working on a internal project for Sky call centres.
Developing a large JavaScript application in an agile environment.
Page 2 of 4

 Technologies include: JavaScript, node.js, nodeunit, QUnit, Backbone.js, HTML5, Socket.io (Websockets), Vows (BDD), XML, TCP, CI (Hudson), Git
John Wiley and Sons
UI Developer & Consultant
August 2008 - April 2011 (2 years 9 months)
Working as a front end developer on the next generation platform to replace Wiley Interscience. http://onlinelibrary.wiley.com
Technologies include: jQuery, HTML, CSS, XML, XSLT, FreeMarker, Selenium, MVN, SVN, Git
ResearchResearch Ltd.
UI Developer & Consultant
March 2007 - July 2008 (1 year 5 months)
Consulted as a front end developer for research professional a repository for news and funding information for research professionals.
Technologies include: jQuery, HTML, CSS, Apache Wicket, Ant, SVN
John Wiley & Sons
Web Developer - User Centred Design March 2005 - March 2007 (2 years 1 month)
Lead user interface developer for Wiley Interscience.
Technologies include: JavaScript, HTML, CSS, jQuery, XML, XSLT, Apache Cocoon, JSP, CVS
ebc Ltd.
Flash/AJAX Developer 2004 - 2005 (1 year)
Working as a lead developer on a number of e-learning systems for clients such as NHS, Barclays Capital, BAT and Network Rail.
Technologies include: HTML (Accessible e-learning), JavaScript, CSS and Adobe Flash
 Education
Page 3 of 4

 University of Lincoln
BSc, Computing, Internet Technologies � (2000 - 2003)
Page 4 of 4
