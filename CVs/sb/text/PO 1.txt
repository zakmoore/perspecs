Zak Moore BSc MBA CMgr MCMI MBCS IIBA
Certified Product Owner, Certified Scrum Master
12 Horns Lodge Farm, Shipbourne Rd, Tonbridge, Kent, TN11 9NE Zak.Moore@gmail.com 07780 998813 @MrZak
CSM CPO
PROFILE:
Product Owner, Agile Project Manager, Enterprise Architect, Business Analyst focusing on value. Experienced Chartered Manager specialising in strategy, innovation, creativity and software delivery. I have led teams from 8 to 65 people delivering stakeholder needs using transparent metrics. Certified Product Owner and Scrum Master
PRINCE Qualified Project Manager (Scottish Project and Engineering Consultancy 1987)
Familiar with Traditional, Agile and Smart-Creative project methods and processes.
Innovation thought leader. Published �Creative Climate Change� in 2014
Skilled user of agile tools such as JIRA, TFS, Rally, QfD and integration with corporate MIS.
           WORK EXPERIENCE:
Time And Emotion
Deliverables
FlowTracker
Deliverables Responsibilities
Open University
Deliverable Responsibilities
Product Owner Apr 16 � Present
Website, CX, Training and Presentations.
Specified, Designed, Build and Deployed Synth Slack bot.
Scheduled User Acceptability Testing and improved on fast feedback.
Designed the Smart Creative Framework to improve efficiency, risk and learning. A4$ way of working for CX oriented strategists, teams and individual
Product Owner/Designer/Developer Jan 16 - Apr 16
Knowledge Worker app available at FlowTracker.org
Wrote prototype in Python and D3.js Web API to produce a tested, working spec. Coded IOs version in C#/Xamarin
Carried out BDD and complemented that with UAT
Scheduled �Jazz� sprints with loose specs for trusted and talented developer. Prepared, packaged and published the apps. Deployed on Google Play and iTunes. Created video for educational purposes
Carried out marketing campaign in conjunction with OU and others.
Product Owner Aug 15 - Mar 16
Creative Technique Library
Designed, built and delivered Creative Technique Library for MBA and external use. Managed stakeholders to secure Intellectual Property Rights.
Implemented modifications to meet commercial needs, rather than academic.
Used by Pfizer and National Blood Transfusion as a performance improvement tool. Used ENO analysis and development to speed delivery and production.
   
TNT Express
Responsibilities:
Lexis Nexis
Deliverables
Agile Project Manager & Subject Matter Expert (Aug 14 � Mar 15)
�Designed & implemented 2 speed Governance Model for waterfall and agile approaches covering Process, Technical and Information sectors.
�Worked with Boston Consulting Group to provide strategic vision of target operating model. �Designed Delivery Model in BPMN with P3M, MSP, PRINCE 2, DSDM and Scrum frameworks �Managed stakeholders with communication plan, workshops and one-on-one meetings. �Carried out Role mapping, Impact analysis and training needs for 54 new CSM & CPOs �Improved morale and risk taking attitude using creative climate change method.
�Ran agile workshops in Amsterdam to integrate Design Agency into TNT Governance. �Coached scrum teams, supported managers and scrum masters to implement agile.
Producy Owner/Agile Project Manager (Sep 13 � May 14)
Increased scrum productivity by implementing innovation techniques.
Improved creative climate - measurably
Implemented re-use of design material
Delivery of UML Designs for Search Engines, online calculators and schedule of loss tools. Managed offshore developers
Measured climate for Innovation. Analysed the dimensions, designed changes to work practices, implemented changes, re-measured the climate.
� Fed Ex days: Introduced 24 hour delivery of apps by groups of workers in guerrilla mode.
� Code Dojos: Mentored and monitored code dojos to share tacit knowledge.
� Creative Problem Solving Techniques: Got people working outside their comfort zones.
� Wrote article for Global Innovation newsletter as requested by VP Innovation of parent company.
ReedElsevier AgileProjectManager-Publishing
Deliverables Change programme to introduce Agile into a waterfall environment. Design attributes of �45M Editorial publishing system
   Agile Project Manager � Front Office Energy Trading (ALM) (Oct 12 � May 13) Deliverables Dodd Frank regulatory upgrade
Renewable energy trading instrument
Replaced scrum with Scrumban to reflect ALM: full ceremonies, team-boards and RAID reports.
Liaised with product owners and architects to achieve Definition of Ready to prove backlog grooming. Introduced story harness with cucumber and NFR recording aspects to improve grooming.
Analysed team performance (BHE). Subsequently facilitated improvements in culture and productivity. Liaised with senior managers to measure Corporate Value Chain by competency and defined TO BE targets for team members. Coached team members 1 on 1 to achieve their competency improvements. Ran distributed agile between London and Norway.
Introduced TDD and pair programming through Code Dojos.
Used A3 experiments to show measurable improvement in work practices and craft skills.
On request by Senior Managers I applied the Wicked Agile Framework from a strategic perspective to pinpoint problems in the support team and guidance on how to resolve them.
Integrated sprint output metrics with ITIL SLA reporting.
Statoil
          Implemented scrum: full ceremonies, team boards and RAID reports.
Inducted and facilitated two multinational cross-business scrum teams using BHE and Kotter. Planned and ran workshops to gather BDD user requirements to generate product backlog. Facilitated empowerment of teams and delivery of design attributes as sprint outputs. Validated offshore knowledge transfer to ensure designs were understood prior to build. Liaised with MD to align technical effort to strategic requirements. Reported through QfD.
Sapient Nitro Agile Project Manager - Digital Media (Apr 11 � Aug 11)
Deliverables Managed creation of a Tender for teleco support contract
Ladbrokes TV, web and m-commerce advertising campaign. Game On!
Implemented scrum to CMMI Level 3: Full ceremonies and team-boards.
On-boarded culturally diverse team using BHE to tender for a multi million pound contract. Ran Code Dojos to introduce Pair Programming.
Managed Ladbrokes TV, print, web and mobile advertising campaign design and build.
(Jan12�Jul12)
          
 DEFRA Agile Project Manager � Government
Deliverables: 3 websites for HM Government Fisheries Agencies
Introduced Kanban to manage web site build.
Mentored and coached 3 developers in CMS manipulation. Sub-contracted graphic design to a creative.
(Oct 10 � Dec 12)
   Zakobyte Consulting Ltd.
Houses of Parliament
Ascot Insurance
John Brown Media
Electrical Contractors Assoc. Wiring Up Youth Justice
Ace Insurance Beazley plc
Global beach
Civil Aviation Authority Zenith Optimedia Ltd. Museek.com
PaperX
Tornado Group plc Republic National Bank FT.com
Reuters
Merrill Lynch
BT
London Underground
SKILLS PROFILE:
Contractor
Jan 1990 � 2010
Jan 10 � Oct 10 Sep 09 � Dec 09 May09�Jun09
Nov 08 - Mar 09
Mar 08 � Oct 08 Aug07�Feb08
Jan 05 � Jul 07 Jun 07 Aug 04 �Dec 04 Dec 04 Nov 03 � Jul 04
Feb03�Nov03 Apr 01 � Feb 03 Sep00�Mar01 Jul 99 � Jul 00 98-99 May 98 � Jun 98 95-98 93-95 93 90-92
 Project Management
Agile
SCRUM
ITIL PERT/PRINCE V Method QA RUP
Years Design Methods & Techniques: Years 13 SSADM 25 10 UML 13
2 OOA/OOD 20 25 Design Patterns (GOF) 18 7 Enterprise Integration Patterns 10 5 SOA 13 3 Refactoring 13 2 TDD 8 BDD 5
12 Deployment & Continuous Integration
18 Cruise control 4 15 Nant 6
20 NUnit/Ncover 9
Kotter Change Mgt
Lean Manufacturing
Languages:
C# 8
Visual Basic Java PERL/Scripting
HTML & Javascript
Government Insurance Digital Media Regulatory Government Insurance Insurance Digital Media Regulatory Media
Music
B2B Portal
Digital Media Investment banking Media
Market Feed Investment banking Telephony Transport
Lead Developer/Architect
Lead Developer/Architect
Agile Evangelist/Developer Scrum Master/Hand on
Agile Evangelist/Developer
Agile Evangelist/Lead Developer Agile Team Leader/Hands on Agile Mentor/Architect
.Net Architect & mentor of 3 Net Architect and analyst Java Developer
Team Leader
Team Leader
Java Developer Implemented FT.com Java Developer
VB Developer
Project Mgt Developer Project Mgt Developer
I have a very good appreciation of strategic and operational business needs. I recognise and understand stakeholder frustrations with software development. I can bridge the communication gap both ways: to help technical teams focus on what is really required as well as sell the forthcoming benefits to stakeholders during periods of process adoption when productivity falls as teams learn to be agile or change to better ways of work.
Through the three phases of my career: trainee, hands on technologist and agile manager I have acquired an impressive set of skills and experience that are easiest to display in the tables below.

XML 20 Version Control 10 Python 17 Jenkins/Hudson 2 SQL 22 Git 2
EDUCATION AND VOCATIONAL QUALIFICATIONS:
Cmgr MCMI (2014)
Certified Product Owner (2013)
NLP Practitioner (2012)
Member IIBA (2012)
MBA (Open 2011)
Certified Scrum Master (2008)
Member British Computing Society (since 2002)
Member of Association for Project Management (1988 � 2006) Territorial Army [REME] (1987-89)
BSc Electrical and Mechanical Engineering (Edinburgh 1984)
References are available on request.
           