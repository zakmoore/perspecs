Chapter 1: Introduction
1.1 Purpose of the BABOK® Guide 1
1.2 What is Business Analysis? 2
1.3 Who is a Business Analyst? 2
1.4 Structure of the BABOK® Guide 3
Chapter 2: Business Analysis Key Concepts
2.1 The Business Analysis Core Concept ModelTM 12
2.2 Key Terms 14
2.3 Requirements Classification Schema 16
2.4 Stakeholders 16
2.5 Requirements and Designs 19
Chapter 3: Business Analysis Planning and Monitoring
3.1 Plan Business Analysis Approach 24
3.2 Plan Stakeholder Engagement 31
3.3 Plan Business Analysis Governance 37
3.4 Plan Business Analysis Information Management 42
3.5 Identify Business Analysis Performance Improvements 47
Chapter 4: Elicitation and Collaboration
4.1 Prepare for Elicitation 56
4.2 Conduct Elicitation 61
4.3 Confirm Elicitation Results 65
4.4 Communicate Business Analysis Information 67
4.5 Manage Stakeholder Collaboration 71
Chapter 5: Requirements Life Cycle Management
5.1 Trace Requirements 79
5.2 Maintain Requirements 83
5.3 Prioritize Requirements 86
5.4 Assess Requirements Changes 91
5.5 Approve Requirements 95
Chapter 6: Strategy Analysis
6.1 Analyze Current State 103
6.2 Define Future State 110
6.3 Assess Risks 120
6.4 Define Change Strategy 124
Chapter 7: Requirements Analysis and Design Definition
7.1 Specify and Model Requirements 136
7.2 Verify Requirements 141
7.3 Validate Requirements 144
7.4 Define Requirements Architecture 148
7.5 Define Design Options 152
7.6 Analyze Potential Value and Recommend Solution 157
Chapter 8: Solution Evaluation
8.1 Measure Solution Performance 166
8.2 Analyze Performance Measures 170
8.3 Assess Solution Limitations 173
8.4 Assess Enterprise Limitations 177
8.5 Recommend Actions to Increase Solution Value 182
Chapter 9: Underlying Competencies
9.1 Analytical Thinking and Problem Solving 188
9.2 Behavioural Characteristics 194
9.3 Business Knowledge 199
9.4 Communication Skills 203
9.5 Interaction Skills 207
9.6 Tools and Technology 211
Chapter 10: Techniques
10.1 Acceptance and Evaluation Criteria 217
10.2 Backlog Management 220
10.3 Balanced Scorecard 223
10.4 Benchmarking and Market Analysis 226
10.5 Brainstorming 227
10.6 Business Capability Analysis 230
10.7 Business Cases 234
10.8 Business Model Canvas 236
10.9 Business Rules Analysis 240
10.10 Collaborative Games 243
10.11 Concept Modelling 245
10.12 Data Dictionary 247
10.13 Data Flow Diagrams 250
10.14 Data Mining 253
10.15 Data Modelling 256
10.16 Decision Analysis 261
10.17 Decision Modelling 265
10.18 Document Analysis 269
10.19 Estimation 271
10.20 Financial Analysis 274
10.21 Focus Groups 279
10.22 Functional Decomposition 283
10.23 Glossary 286
10.24 Interface Analysis 287
10.25 Interviews 290
10.26 Item Tracking 294
10.27 Lessons Learned 296
10.28 Metrics and Key Performance Indicators (KPIs) 297
10.29 Mind Mapping 299
10.30 Non-Functional Requirements Analysis 302
10.31 Observation 305
10.32 Organizational Modelling 308
10.33 Prioritization 311
10.34 Process Analysis 314
10.35 Process Modelling 318
10.36 Prototyping 323
10.37 Reviews 326
10.38 Risk Analysis and Management 329
10.39 Roles and Permissions Matrix 333
10.40 Root Cause Analysis 335
10.41 Scope Modelling 338
10.42 Sequence Diagrams 341
10.43 Stakeholder List, Map, or Personas 344
10.44 State Modelling 348
10.45 Survey or Questionnaire 350
10.46 SWOT Analysis 353
10.47 Use Cases and Scenarios 356
10.48 User Stories 359
10.49 Vendor Assessment 361
10.50 Workshops 363
Chapter 11: Perspectives
11.1 The Agile Perspective 368
11.2 The Business Intelligence Perspective 381
11.3 The Information Technology Perspective 394
11.4 The Business Architecture Perspective 408
11.5 The Business Process Management Perspective 424