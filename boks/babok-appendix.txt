Appendix A: Glossary


acceptance criteria: Criteria associated with requirements, products, or the 
delivery cycle that must be met in order to achieve stakeholder acceptance.

actor (business analysis): A human, device, or system that plays some specified 
role in interacting with a solution.

adaptive approach: An approach where the solution evolves based on a cycle of 
learning and discovery, with feedback loops which encourage making 
decisions as late as possible.

Agile Extension to the BABOK® Guide: A standard on the practice of business 

analysis in an agile context. The Agile Extension to the BABOK® Guide 
version 1 was published in 2013 by IIBA®, in partnership with the Agile 
Alliance.

allocation: See requirements allocation.
architecture: The design, structure, and behaviour of the current and future states 

of a structure in terms of its components, and the interaction between 
those components. See also business architecture, enterprise architecture, 
and requirements architecture.

artifact (business analysis): Any solution-relevant object that is created as part of 

business analysis efforts.

assumption: An influencing factor that is believed to be true but has not been 

confirmed to be accurate, or that could be true now but may not be in the 
future.

behavioural business rule: A business rule that places an obligation (or 

prohibition) on conduct, action, practice, or procedure; a business rule 
whose purpose is to shape (govern) day-to-day business activity. Also 
known as operative rule.

benchmarking: A comparison of a decision, process, service, or system's cost, 

time, quality, or other metrics to those of leading peers to identify 
opportunities for improvement.

body of knowledge: The aggregated knowledge and generally accepted practices 

on a topic.

BPM: See business process management.
brainstorming: A team activity that seeks to produce a broad or diverse set of 

options through the rapid and uncritical generation of ideas.

business (business analysis): See enterprise.
business (business world): An economic system where any commercial, industrial, 
or professional activity is performed for profit.



business analysis: The practice of enabling change in the context of an enterprise 
by defining needs and recommending solutions that deliver value to 
stakeholders.

business analysis information: Any kind of information at any level of detail that is 

used as an input to business analysis work, or as an output of business 
analysis work.

business analysis package: A document, presentation, or other collection of text, 
matrices, diagrams and models, representing business analysis information.

business analyst: Any person who performs business analysis, no matter their job 
title or organizational role. For more information, see Who is a Business 
Analyst? (p. 2).

business analysis approach: The set of processes, rules, guidelines, heuristics, and 
activities that are used to perform business analysis in a specific context.

business analysis communication plan: A description of the types of 
communication the business analyst will perform during business analysis, 
the recipients of those communications, and the form and frequency of 
those communications.

business analysis effort: The scope of activities a business analyst is engaged in 
during the life cycle of an initiative.

business analysis plan: A description of the planned activities the business analyst 
will execute in order to perform the business analysis work involved in a 
specific initiative. See also requirements management plan.

business architecture: The design, structure, and behaviour of the current and 
future states of an enterprise to provide a common understanding of the 
organization. It is used to align the enterprise’s strategic objectives and 
tactical demands.

business case: A justification for a course of action based on the benefits to be 

realized by using the proposed solution, as compared to the cost, effort, and 
other considerations to acquire and live with that solution.

business decision: A decision that can be made based on strategy, executive 
judgment, consensus, and business rules, and that is generally made in 
response to events or at defined points in a business process.

business domain: See domain.

business goal: A state or condition that an organization is seeking to establish 
and maintain, usually expressed qualitatively rather than quantitatively.

business need: A problem or opportunity of strategic or tactical importance to be 
business objective: An objective, measurable result to indicate that a business 

business policy: A non-practicable directive that controls and influences the actions of an enterprise.

addressed.

goal has been achieved.



business problem: An issue of strategic or tactical importance preventing an 
enterprise or organization from achieving its goals.

business process: An end-to-end set of activities which collectively responds to an 
event, and transforms information, materials, and other resources into 
outputs that deliver value directly to the customers of the process. It may be 
internal to an organization, or it may span several organizations.

business process management (BPM): A management discipline that determines 
how manual and automated processes are created, modified, cancelled, 
and governed.

business process re-engineering: Rethinking and redesigning business processes 
to generate improvements in performance measures.

business requirement: A representation of goals, objectives and outcomes that 
describe why a change has been initiated and how success will be assessed.

business rule: A specific, practicable, testable directive that is under the control of 
the business and that serves as a criterion for guiding behaviour, shaping 
judgments, or making decisions.

capability: The set of activities the enterprise performs, the knowledge it has, the 
products and services it provides, the functions it supports, and the methods 
it uses to make decisions.

cause-and-effect diagram: See fishbone diagram.

change: The act of transformation in response to a need.

change agent: One who is a catalyst for change.

change control: Controlling changes to requirements and designs so that the 

impact of requested changes is understood and agreed-to before the 
changes are made.

change management: Planned activities, tools, and techniques to address the 

human side of change during a change initiative, primarily addressing the 
needs of the people who will be most affected by the change.

change strategy: A plan to move from the current state to the future state to 
achieve the desired business objectives.

change team: A cross-functional group of individuals who are mandated to 
implement a change. This group may be comprised of product owners, 
business analysts, developers, project managers, implementation subject 
matter experts (SMEs), or any other individual with the relevant set of skills 
and competencies required to implement the change.

checklist (business analysis): A standard set of quality elements that reviewers use 
for requirements verification.

collaboration: The act of two or more people working together towards a 
common goal.

commercial off-the-shelf (COTS): A prepackaged solution available in the 

marketplace which address all or most of the common needs of a large 
group of buyers of those solutions. A commercial off-the-shelf solution may 
require some configuration to meet the specific needs of the enterprise.

competitive analysis: A structured assessment which captures the key 
characteristics of an industry to predict the long-term profitability prospects 
and to determine the practices of the most significant competitors.

component: A uniquely identifiable element of a larger whole that fulfills a clear 
function.

concept model: An analysis model that develops the meaning of core concepts 
for a problem domain, defines their collective structure, and specifies the 
appropriate vocabulary needed to communicate about it consistently.

constraint (business analysis): An influencing factor that cannot be changed, and 
that places a limit or restriction on a possible solution or solution option.

context: The circumstances that influence, are influenced by, and provide 
understanding of the change.

core concept (business analysis): One of six ideas that are fundamental to the 
practice of business analysis: Change, Need, Solution, Context, Stakeholder, 
and Value.

cost-benefit analysis: An analysis which compares and quantifies the financial and 
non-financial costs of making a change or implementing a solution 
compared to the benefits gained.

COTS: See commercial off-the-shelf.

CRUD matrix: See create, read, update, and delete matrix.
create, read, update, and delete matrix (CRUD matrix): A two-dimensional matrix 
showing which user roles have permission to access specific information 
entities, and to create new records in those entities, view the data in existing 
records, update or modify the data in existing records, or delete existing 
records. The same type of matrix can be used to show which processes, 
instead of users, have the create, read, update and delete rights.

customer: A stakeholder who uses or may use products or services produced by 
the enterprise and may have contractual or moral rights that the enterprise 
is obliged to meet.

decision analysis: An approach to decision making that examines and models the 
possible consequences of different decisions, and assists in making an 
optimal decision under conditions of uncertainty.

decomposition: A technique that subdivides a problem into its component parts 
in order to facilitate analysis and understanding of those components.

defect: A deficiency in a product or service that reduces its quality or varies from 
a desired attribute, state, or functionality.


definitional business rule: A rule that indicates something is necessarily true (or 
untrue); a rule that is intended as a definitional criterion for concepts, 
knowledge, or information. Also known as a structural rule.

deliverable: Any unique and verifiable work product or service that a party has 
agreed to deliver.

design: A usable representation of a solution. 

document analysis (business analysis): An examination of the documentation of 
an existing system in order to elicit requirements.

domain: The sphere of knowledge that defines a set of common requirements, 
terminology, and functionality for any program or initiative solving a 
problem.

domain subject matter expert: A stakeholder with in-depth knowledge of a topic 
relevant to the business need or solution scope.

DSDM: See dynamic systems development method.
dynamic systems development method (DSDM): A project delivery framework 

which focuses on fixing cost, quality, and time at the beginning while 
contingency is managed by varying the features to be delivered.

elicitation: Iterative derivation and extraction of information from stakeholders or 
other sources.

end user: A stakeholder who directly interacts with the solution.

enterprise: A system of one or more organizations and the solutions they use to 
pursue a shared set of common goals.

enterprise architecture: A description of the business processes, information 
technology, people, operations, information, and projects of an enterprise 
and the relationships between them.

enterprise readiness assessment: An assessment that describes the enterprise is 

prepared to accept the change associated with a solution and is able to use 
it effectively.

entity-relationship diagram: A graphical representation of the entities relevant to 
a chosen problem domain and the relationships between them.

estimate: A quantitative assessment of a planned outcome, resource 
requirements, and schedule where uncertainties and unknowns are 
systematically factored into the assessment.

evaluation: The systematic and objective assessment of a solution to determine its 
status and efficacy in meeting objectives over time, and to identify ways to 
improve the solution to better meet objectives. See also indicator; metric, 
monitoring.



event (business analysis): An occurrence or incident to which an organizational 
unit, system, or process must respond.

evolutionary prototype: A prototype that is continuously modified and updated in 
response to feedback from stakeholders.

experiment: Elicitation performed in a controlled manner to make a discovery, test 
a hypothesis, or demonstrate a known fact.

external interface: An interaction that is outside the proposed solution. It can be 
another hardware system, software system, or a human interaction with 
which the proposed solution will interact.

facilitation: The art of leading and encouraging people through systematic efforts 
toward agreed-upon objectives in a manner that enhances involvement, 
collaboration, productivity, and synergy.

feasibility study: An evaluation of proposed alternatives to determine if they are 
technically, organizationally, and economically possible within the 
constraints of the enterprise, and whether they will deliver the desired 
benefits to the enterprise.

feature: A distinguishing characteristic of a solution that implements a cohesive 
set of requirements and which delivers value for a set of stakeholders.
fishbone diagram: A diagramming technique used in root cause analysis to 
identify underlying causes of an observed problem, and the relationships 
that exist between those causes. Also known as an Ishikawa or cause-and-
effect diagram.

focus group: A group formed to to elicit ideas and attitudes about a specific 
product, service, or opportunity in an interactive group environment. The 
participants share their impressions, preferences, and needs, guided by a 
moderator.

force field analysis: A graphical method for depicting the forces that support and 
oppose a change. Involves identifying the forces, depicting them on 
opposite sides of a line (supporting and opposing forces) and then 
estimating the strength of each set of forces.

functional requirement: A capability that a solution must have in terms of the 
behaviour and information the solution will manage.

gap analysis: A comparison of the current state and desired future state of an 
enterprise in order to identify differences that need to be addressed.

goal: See business goal.

governance process (change): A process by which appropriate decision makers 

use relevant information to make decisions regarding a change or solution, 
including the means for obtaining approvals and priorities.


guideline (business analysis): An instruction or description on why or how to 
undertake a task.

horizontal prototype: A prototype that is used to explore requirements and 
designs at one level of a proposed solution, such as the customer-facing 
view or the interface to another organization.

impact analysis: An assessment of the effects a proposed change will have on a 
stakeholder or stakeholder group, project, or system.

implementation subject matter expert: A stakeholder who has specialized 

knowledge regarding the implementation of one or more solution 
components.

indicator: A specific numerical measurement that indicates progress toward 
achieving an impact, output, activity, or input. See also metric.

initiative: A specific project, program, or action taken to solve some business 
problem(s) or achieve some specific change objective(s).

input (business analysis): Information consumed or transformed to produce an 
output. An input is the information necessary for a task to begin.
inspection: A formal review of a work product by qualified individuals that follows 
a predefined process, and uses predefined criteria, for defect identification 
and removal.

interface: A shared boundary between any two persons and/or systems through 
which information is communicated.

interoperability: Ability of systems to communicate by exchanging data or 
services.

interview: Eliciting information from a person or group of people in an informal 
or formal setting by asking relevant questions and recording the responses.

Ishikawa diagram: See fishbone diagram.

iteration (business analysis): A single instance of progressive cycles of analysis, 
development, testing, or execution.


knowledge area (business analysis): An area of expertise that includes several 
specific business analysis tasks.

lessons learned process: A process improvement technique used to learn about 
and improve on a process or project. A lessons learned session involves a 
special meeting in which the team explores what worked, what didn't work, 
what could be learned from the just-completed iteration, and how to adapt 
processes and techniques before continuing or starting anew.



life cycle: A series of changes an item or object undergoes from inception to 
retirement

matrix: A textual form of modelling used to represent information that can be 
categorized, cross-referenced, and represented in a table format.

metadata: A description of data to help understand how to use that data, either 
in terms of the structure and specification of the data, or the description of 
a specific instance of an object.

methodology: A body of methods, techniques, procedures, working concepts, 
and rules used to solve a problem

metric: A quantifiable level of an indicator measured at a specified point in time.
mission statement: A formal declaration of values and goals that expresses the 
core purpose of the enterprise.

model: A representation and simplification of reality developed to convey 
information to a specific audience to support analysis, communication, and 
understanding.

monitoring: Collecting data on a continuous basis from a solution in order to 
determine how well a solution is implemented compared to expected 
results. See also metric; indicator.

need: A problem or opportunity to be addressed.

non-functional requirement: A type of requirement that describes the 
performance or quality attributes a solution must meet. Non-functional 
requirements are usually measurable and act as constraints on the design of 
a solution as a whole.

objective: See business objective.

observation (business analysis): Studying and analyzing one or more stakeholders 
in their work environment in order to elicit requirements.

OLAP: See online analytical processing.
online analytical processing (OLAP): A business intelligence approach that allows 
users to analyze large amounts of data from different points of view.
operational support: A stakeholder who is responsible for the day-to-day 
management and maintenance of a system or product.

operative rule: See behavioural business rule.

organization: An autonomous group of people under the management of a 
single individual or board, that works towards common goals and 
objectives.

organizational capability: A function inside the enterprise, made up of 
components such as processes, technologies, and information and used by 
organizations to achieve their goals.

organizational change management: See change management.

organization modelling: The analysis technique used to describe roles, 
responsibilities and reporting structures that exist within an enterprise.
organizational unit: Any recognized association of people within an organization 
or enterprise.

peer review: A formal or informal review of a work product to identify errors or 
opportunities for improvement. See also inspection.

plan: A detailed scheme for doing or achieving something usually comprising a 
set of events, dependencies, expected sequence, schedule, results or 
outcomes, materials and resources needed, and how stakeholders need to 
be involved.

policy: See business policy.

predictive approach: An approach where planning and baselines are established 
early in the life cycle of the initiative in order to maximize control and 
minimize risk.

prioritization: Determining the relative importance of a set of items in order to 
determine the order in which they will be addressed.

process: A set of activities designed to accomplish a specific objective by taking 
one or more defined inputs and turning them into defined outputs.

process model: A set of diagrams and supporting information about a process 
and factors that could influence the process. Some process models are used 
to simulate the performance of the process.

product (business analysis): A solution or component of a solution that is the 
result of an initiative.

product backlog: A set of user stories, requirements, or features that have been 
identified as candidates for potential implementation, prioritized, and 
estimated.

product scope: See solution scope.

product vision statement: A brief statement or paragraph that describes the goals 
of the solution and how it supports the strategy of the organization or 
enterprise.

project: A temporary endeavour undertaken to create a unique product, service, 
or result.

project manager: A stakeholder who is responsible for managing the work 
required to deliver a solution that meets a business need, and for ensuring 
that the project's objectives are met while balancing the project constraints, 
including scope, budget, schedule, resources, quality, and risk.

project scope: The work that must be performed to deliver a product, service, or 
result with the specified features and functions.

proof of concept: A model created to validate the design of a solution without 
modelling the appearance, materials used in the creation of work, or 
processes and workflows ultimately used by the stakeholders.

prototype: A partial or simulated approximation of the solution for the purpose of 
eliciting or verifying requirements with stakeholders.

quality: The degree to which a set of inherent characteristics fulfills needs.
quality assurance: A set of activities performed to ensure that a process will 
deliver products that meet an appropriate level of quality.

quality attributes: A set of measures used to judge the overall quality of a system.
See also non-functional requirements.

questionnaire: A set of defined questions, with a choice of answers, used to 
collect information from respondents.

RACI matrix: See responsible, accountable, consulted, and informed matrix.
regulator: A stakeholder from outside the organization who is responsible for the 
definition and enforcement of standards.

repository: A real or virtual facility where all information on a specific topic is 
stored and is available for retrieval.

request for information (RFI): A formal elicitation method intended to collect 
information regarding a vendor's capabilities or any other information 
relevant to a potential upcoming procurement.

request for proposal (RFP): A requirements document issued when an 
organization is seeking a formal proposal from vendors. An RFP typically 
requires that the proposals be submitted following a specific process and 
using sealed bids which will be evaluated against a formal evaluation 
methodology.

request for quote (RFQ): A procurement method of soliciting price and solution options from vendors.

request for tender (RFT): An open invitation to vendors to submit a proposal for 
goods or services.

requirement: A usable representation of a need.

requirements attribute: A characteristic or property of a requirement used to 
assist with requirements management.


requirements allocation: The process of assigning requirements to be 
implemented by specific solution components.

requirements architecture: The requirements of an initiative and the 
interrelationships between these requirements.

requirements artifact: A business analysis artifact containing information about 
requirements such as a diagram, matrix, document or model.

requirements defect: A problem or error in a requirement. Defects may occur 
because a requirement is poor quality (see requirements verification) or 
because it does not describe a need that, if met, would provide value to 
stakeholders (see requirements validation).

requirements document: See requirements package.

requirements life cycle: The stages through which a requirement progresses from 
inception to retirement.

requirements management: Planning, executing, monitoring, and controlling any 
or all of the work associated with requirements elicitation and collaboration, 
requirements analysis and design, and requirements life cycle management.

requirements management plan: A subset of the business analysis plan for a 
specific change initiative, describing specific tools, activities, and roles and 
responsibilities that will be used on the initiative to manage the 
requirements. See business analysis plan.

requirements management tool: Special-purpose software that provides support 
for any combination of the following capabilities: elicitation and 
collaboration, requirements modelling and/or specification, requirements 
traceability, versioning and baselining, attribute definition for tracking and 
monitoring, document generation, and requirements change control.

requirements model: An abstract (usually graphical) representation of some 
aspect of the current or future state.

requirements package: A specialized form of a business analysis package 

primarily concerned with requirements. A requirements package may 
represent a baseline of a collection of requirements.

requirements traceability: The ability for tracking the relationships between sets 
of requirements and designs from the original stakeholder need to the 
actual implemented solution. Traceability supports change control by 
ensuring that the source of a requirement or design can be identified and 
other related requirements and designs potentially affected by a change are 
known.

requirements validation: Work done to evaluate requirements to ensure they 
support the delivery of the expected benefits and are within the solution 
scope.

requirements verification: Work done to evaluate requirements to ensure they are 
defined correctly and are at an acceptable level of quality. It ensures the 
requirements are sufficiently defined and structured so that the solution 
development team can use them in the design, development, and 
implementation of the solution.

requirements workshop: A structured meeting in which a carefully selected group 
of stakeholders collaborate to define and/or refine requirements under the 
guidance of a skilled neutral facilitator.

residual risk: The risk remaining after action has been taken or plans have been 
put in place to deal with the original risk.

retrospective: See lessons learned process.

return on investment (ROI) (business analysis): A measure of the profitability of a 
project or investment.

responsible, accountable, consulted, and informed matrix (RACI matrix): A tool 
used to identify the responsibilities of roles or team members and the 
activities or deliverables in which they will participate, by being responsible 
(doing the work), accountable (approving the results), consulted (providing 
input) or informed of the completed item after it has been completed.

RFI:  See request for information.

RFP:  See request for proposal.

RFQ: See request for quote.

RFT:  See request for tender.

risk (business analysis): The effect of uncertainty on the value of a change, a 
solution, or the enterprise. See also residual risk.

risk assessment: Identifying, analyzing and evaluating risks.

ROI:  See return on investment.

root cause: The cause of a problem having no deeper cause, usually one of 

several possible causes.

root cause analysis: A structured examination of an identified problem to 
understand the underlying causes.

scope: The boundaries of control, change, a solution, or a need.

scope model: A model that defines the boundaries of a business domain or solution.

secondary actor: An actor external to the system under design that supports the 
execution of a use case.

sequence diagram: A type of diagram that shows objects participating in 
interactions and the messages exchanged between them.

service (business analysis): The performance of any duties or work for a 
stakeholder, from the perspective of the stakeholder.

SIPOC: See suppliers, inputs, process, outputs and customers.


SME: See subject matter expert.

software engineer: See developer.

solution: A specific way of satisfying one or more needs in a context.

solution component: A sub-part of a solution that can be people, infrastructure, 
hardware, software, equipment, facilities, and process assets or any 
combination of these sub-parts.

solution option: One possible way to satisfy one or more needs in a context.

solution requirement: A capability or quality of a solution that meets the 
stakeholder requirements. Solution requirements can be divided into two 
sub-categories: functional requirements and non-functional requirements or 
quality of service requirements.

solution life cycle: The stages through which a solution progresses from inception to retirement. 

solution scope: The set of capabilities a solution must deliver in order to meet the 
business need.

SOW: See statement of work.

sponsor: A stakeholder who is responsible for initiating the effort to define a 
business need and develop a solution that meets that need. They authorize 
the work to be performed and control the budget and scope for the 
initiative.

stakeholder: A group or individual with a relationship to the change, the need, or 
the solution.

stakeholder analysis: Identifying and analyzing the stakeholders who may be 
impacted by the change and assess their impact, participation, and needs 
throughout the business analysis activities.

stakeholder list: A catalogue of the stakeholders affected by a change, business 
need, or proposed solution, and a description of their attributes and 
characteristics related to their involvement in the initiative.

stakeholder proxy (business analyst): The role a business analyst takes when 
representing the needs of a stakeholder or stakeholder group.

stakeholder requirement: A description of the needs of a particular stakeholder or 
class of stakeholders that must be met in order to achieve the business 
requirements. They may serve as a bridge between business requirements 
and the various categories of solution requirements.

state diagram: An analysis model showing the life cycle of a data entity or class.

stated requirement: A requirement articulated by a stakeholder that has not been 
analyzed, verified, or validated. Stated requirements frequently reflect the 
desires of a stakeholder rather than the actual need.

statement of work (SOW): A written description of the services or tasks that are 
required to be performed.

strategy: A description of the chosen approach to apply the capabilities of an 
enterprise in order to reach a desired set of goals or objectives.

strengths, weaknesses, opportunities, and threats analysis (SWOT): An analysis 
model used to understand influencing factors and how they may affect an 
initiative. Also known as SWOT analysis.

structural rule: See definitional business rule.

subject matter expert (SME): See domain subject matter expert; implementation 
subject matter expert.

supplier: A stakeholder outside the boundary of a given organization or 

organizational unit who provides products or services to the organization 
and may have contractual or moral rights and obligations that must be 
considered.

suppliers, inputs, process, outputs, and customers (SIPOC): A tool used to 
describe relevant high-level elements of a process. May be used in 
conjunction with process mapping and ‘in/out of scope’ tools, to provide 
additional detail.

survey: Collecting and measuring the opinions or experiences of a group of 
people through a series of questions.

swimlane: A horizontal or vertical section of a process diagram that shows which 
activities are performed by a particular actor or role.

SWOT analysis: See strengths, weaknesses, opportunities and threats analysis.
system: A set of interdependent components that interact in various ways to 
produce a set of desired outcomes.

task (business analysis): A discrete piece of work that may be performed formally 
or informally as part of business analysis.

technique: A manner, method, or style for conducting a business analysis task or 
for shaping its output.

temporal event: An event based on time that can trigger the initiation of a 
process, evaluation of business rules, or some other response.

tester: An individual responsible for determining how to verify that the solution 
meets the requirements defined by the business analyst, and conducting the 
verification process.

throw-away prototype: A prototype used to quickly uncover and clarify 
requirements or designs using simple tools, sometimes just paper and 
pencil. It is intended to be discarded when the final system has been 
developed.

time-box: An agreed-upon period of time in which an activity is conducted or a 
defined deliverable is intended to be produced.

traceability: See requirements traceability.

transition requirement: A requirement that describes the capabilities the solution 
must have and the conditions the solution must meet to facilitate transition 
from the current state to the future state, but which are not needed once 
the change is complete. They are differentiated from other requirements 
types because they are of a temporary nature.

UAT:  See user acceptance test.
UML: See unified modelling language.
unified modelling language™ A notation specified by the Object Management 
Group for describing software application structure, behaviour, and 
architecture. It can also be used for describing business processes and data 
structures. The most common UML® diagrams used by business analysts are 
use case diagrams, activity diagrams, state machine diagrams (also known 
as state diagrams), and class diagrams.

use case: A description of the observable interaction between an actor (or actors) 
and a solution that occurs when the actor uses the system to accomplish a 
specific goal.

use case diagram: A type of diagram defined by UML® that captures all actors 
and use cases involved with a system or product.

user: See end user.

user acceptance test (UAT): Assessing whether the delivered solution meets the 
needs of the stakeholder group that will be using the solution. The 
assessment is validated against identified acceptance criteria.

user requirement: See stakeholder requirement.

user story: A small, concise statement of functionality or quality needed to deliver 
value to a specific stakeholder.

validation (business analysis): The process of checking that a deliverable is 
suitable for its intended use. See also requirements validation.

validated requirement: A requirement that has been reviewed and is determined 
to support the delivery of the expected benefits, and is within the solution 
scope.

value (business analysis): The worth, importance, or usefulness of something to a 
stakeholder in a context.

value stream mapping: A complete, fact-based, time-series representation of the 
stream of activities required to deliver a product or service.

verification (business analysis): The process of determining that a deliverable or 

artifact meets an acceptable standard of quality. See also requirements 
verification.

verified requirement: A requirement that has been reviewed and is determined to 
be defined correctly, adheres to standards or guidelines, and is at an 
acceptable level of detail.

vertical prototype: A prototype that is used to drill down into a proposed solution 
to uncover requirement and design considerations through multiple layers 
of a solution that are not easily understood or that are not discernible on 
the surface. It may include interaction between several solution 
components.

viewpoint: A set of conventions that define how requirements will be 
represented, how these representations will be organized, and how they 
will be related.

VSM: See value stream mapping.

walkthrough: A review in which participants step through an artifact or set of 
artifacts with the intention of validating the requirements or designs, and to 
identify requirements or design errors, inconsistencies, omissions, 
inaccuracies, or conflicts.

WBS: See work breakdown structure.

work breakdown structure (WBS): A deliverable-oriented hierarchical 
decomposition of the work to be executed to accomplish objectives and 
create the required deliverables. It organizes and defines the total scope of 
the project.

work product (business analysis): A document or collection of notes or diagrams 
used by the business analyst during the requirements development process.

Workshop: A facilitated and focused event attended by key stakeholders for the 


